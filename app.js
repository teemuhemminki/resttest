//Haetaan moduulit käyttöön
var express = require('express');
var bodyParser = require('body-parser');

//Luodaan express appi
const app = express();

//Asetetaan appi parsettamaan "application/x-www-form-urlencoded" sisällöt
app.use(bodyParser.urlencoded({ extended: true }))

//Asetetaan appi parsettamaan "application/json" sisällöt
app.use(bodyParser.json())

//Konfiguroidaan tietokanta
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.connect(dbConfig.url)
    .then(() => {
        console.log('Successfully connected to the database');
    }).catch(err => {
        console.log('Could not connect to the database. Exiting now..');
        process.exit();
    });

//Haetaan reititykset
require('./routes/opiskelija.routes.js')(app);

//Asetetaan appi kuuntelemaan pyyntöjä
app.listen(3000, function() {
    console.log('app running on port. 3000');
});
